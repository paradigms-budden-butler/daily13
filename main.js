console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var country = document.getElementById("name-text").value;
    console.log('Country you entered is ' + country);
    makeNetworkCallToCountryApi(country);

} // end of get form info

function makeNetworkCallToCountryApi(country){
    console.log('entered make nw call' + country);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://universities.hipolabs.com/search?country=" + country;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateCountryWithResponse(country, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateCountryWithResponse(country, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");

    if(response_json == null){
        label1.innerHTML = 'Apologies, we could not find your country'
    } else{
        label1.innerHTML =  country + " has  " + response_json.length + " Universities.";
		var num = response_json.length;
        makeNetworkCallToNumbers(num);
    }
} // end of updateAgeWithResponse

function makeNetworkCallToNumbers(num){
    console.log('entered make nw call' + num);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "http://numbersapi.com/" + num;
    xhr.open("GET", url, true) // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateTriviaWithResponse(num, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updateTriviaWithResponse(num, response_text){
    // update a label
    var label2 = document.getElementById("response-line2");
    label2.innerHTML = response_text;

} // end of updateTriviaWithResponse
